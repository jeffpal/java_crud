/*
# Copyright (C) 2019 Engineer Jeff Pal. All rights reserved.
*/

package model;

public class Person {

    public Person() {

    }

    private int id;
    private String name;
    private String email;
    private String gender;
    private String born;
    private String phone;
    private String originChannel;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
 
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    
    public String getBorn() {
        return born;
    }

    public void setBorn(String born) {
        this.born = born;
    }
    
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOriginChannel() {
        return originChannel;
    }

    public void setOriginChannel(String originChannel) {
        this.originChannel = originChannel;
    }
}
