/*
# Copyright (C) 2019 Engineer Jeff Pal. All rights reserved.
*/

package dao;

//import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
import java.sql.SQLException;

public class DataBase {
    
    private Connection connection;
    private String url = "jdbc:postgresql://localhost:5432/leaderleads";
    private String user = "postgres";
    private String password = "0546";

    public Connection getConnection() {
        
        try {

        Class.forName("org.postgresql.Driver");

        } catch(ClassNotFoundException e ){
        e.getMessage();
        }

        try {
        connection = DriverManager.getConnection(url, user, password);
        System.out.println("Connected");
//        JOptionPane.showMessageDialog(null, "Connected");
        } catch (SQLException ex) {
        Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        JOptionPane.showMessageDialog(null, "Failed To Connect");
        }

        return connection;
        
    }       
}
