/*
# Copyright (C) 2019 Engineer Jeff Pal. All rights reserved.
*/

package dao;

import javax.swing.JOptionPane;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Person;

public class DataAccessObject {

    private Connection connection;

    public DataAccessObject() {
        DataBase con = new DataBase();
        connection = con.getConnection();
    }

    public void addPerson(Person person) {
        try {
            String query = "insert into public.leads(nome, email, sexo, data_nascimento, telefone, canal_origem) values ('" + person.getName() + "', '" + person.getEmail() + "', '" + person.getGender() + "', '" + person.getBorn() + "', '" + person.getPhone() + "', '" + person.getOriginChannel() + "')";

            Statement stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removePerson(int personid) {
        String query = "delete from public.leads where public.leads.id = " + personid + " ";
        try {
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updatePerson(Person person) {
        String query;
        query = "update public.leads set nome='" + person.getName() + "', email='" + person.getEmail()
                + "', sexo='" + person.getGender()
                + "', data_nascimento='" + person.getBorn()
                + "', telefone='" + person.getPhone()
                + "', canal_origem='" + person.getOriginChannel()
                + "' where id = " + person.getId() + " ";
        System.out.println(query);
        try {
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Person> getPersons() throws SQLException {
        String query = "select * from public.leads";
        ArrayList<Person> persons = new ArrayList<>();
        Statement stmt = connection.createStatement();
        ResultSet res = stmt.executeQuery(query);
        while (res.next()) {
            Person person = new Person();
            person.setId(res.getInt("id"));
            person.setName(res.getString("nome"));
            person.setEmail(res.getString("email"));
            person.setGender(res.getString("sexo"));
            person.setBorn(res.getString("data_nascimento"));
            person.setPhone(res.getString("telefone"));
            person.setOriginChannel(res.getString("canal_origem"));
            
            persons.add(person);
        }
        return persons;
    }

    public Person getPersonById(int personid) throws SQLException {
        Person person = new Person();
        String query = "select * from public.leads where public.leads.id = " + personid + " ";
        Statement stmt = connection.createStatement();
        ResultSet res = stmt.executeQuery(query);
        if (res.next()) {
            person.setId(res.getInt("id"));
            person.setName(res.getString("nome"));
            person.setEmail(res.getString("email"));
            person.setGender(res.getString("sexo"));
            person.setBorn(res.getString("data_nascimento"));
            person.setPhone(res.getString("telefone"));
            person.setOriginChannel(res.getString("canal_origem"));
        }
        return person;
    }
}
