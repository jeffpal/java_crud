<!--
# Copyright (C) 2019 Engineer Jeff Pal. All rights reserved.
-->

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
        <mvc:resources location="/files/" mapping="/files/**"></mvc:resources>
        <link rel="icon" href='<c:url value="/files/ll_logo.png"></c:url>' type="image/png">
    </head>
    <body> 
        <jsp:forward page="/PersonController?action=listperson" />
    </body>
</html>