<!--
# Copyright (C) 2019 Engineer Jeff Pal. All rights reserved.
-->

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>

<html lang="en">
<head>
		
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">

<title>Leads Book</title>

<link href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
<link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href="bootstrap.css" rel="stylesheet">
<mvc:resources location="/files/" mapping="/files/**"></mvc:resources>
<link rel="shortcut icon" href='<c:url value="/files/ll_logo.png"></c:url>' type="image/png">
</head>
	<body>
	<div class="wrap">
	<section>
            <div class="container">
                    <center><img src='<c:url value="/files/ll_logo.png"></c:url>' vspace="50"/></center>
                    <table class="table table-hover">
                            <thead>
                                    <tr>
                                            <th>
                                                    #
                                            </th>
                                            <th>
                                                    Nome
                                            </th>
                                            <th>
                                                    Email
                                            </th>
                                            <th>
                                                    Sexo
                                            </th>
                                            <th>
                                                    Data de Nascimento
                                            </th>
                                            <th>
                                                    Telefone
                                            </th>
                                            <th>
                                                    Canal de origem
                                            </th>
                                    </tr>
                            </thead>
                            <tbody>
                                    <c:forEach items="${persons}" var="person">
                                            <tr>
                                                    <td>
                                                            <c:out value="${person.id}"/>
                                                    </td>
                                                    <td>
                                                            <c:out value="${person.name}"/>
                                                    </td>
                                                    <td>
                                                            <c:out value="${person.email}"/>
                                                    </td>
                                                    <td>
                                                            <c:out value="${person.gender}"/>
                                                    </td>
                                                    <td>
                                                            <c:out value="${person.born}"/>
                                                    </td>
                                                    <td>
                                                            <c:out value="${person.phone}"/>
                                                    </td>
                                                    <td>
                                                            <c:out value="${person.originChannel}"/>
                                                    </td>
                                                    <td><a href="PersonController?action=edit&id=<c:out value="${person.id}"/>">Update</a></td>
                                                    <td><a href="PersonController?action=delete&id=<c:out value="${person.id}"/>">Delete</a></td>
                                            </tr>
                                    </c:forEach>
                            </tbody>
                    </table>
            <a href="PersonController?action=insert" role="button" class="btn btn-info btn-lg" data-toggle="modal">Add new person</a>	
            </div>
	</section>	
</div>

<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

</body>
</html>